# WorkdayCalendar
[![CI/CD](https://github.com/codemascot/WorkdayCalendar/actions/workflows/ci-cd.yaml/badge.svg)](https://github.com/codemascot/WorkdayCalendar/actions/workflows/ci-cd.yaml)

A project to calculate the workday time.

## Packages & Tools
- Arch Linux
- **.NET v7.0.103** 🎯 **net7.0**
- Moq
- Xunit

## Running & Test
As this is a class library there is no direct ways to use it unless we use this in a project. Therefore I have leverage the TDD approach to test the functionality of this class library.

Anyway, here 28 tests has been written with `Xunit` for this whole project and `Moq` package used to mock some dependencies. Running the tests were pretty straight forward in my Linux with `dotnet test` command!

![Screenshot](Screenshot_Test.png)

Another thing, I found the arguments [here by James Newkirk](https://jamesnewkirk.typepad.com/posts/2007/09/why-you-should-.html) quite compelling. Therefore tried to isolate the tests, rather setting up in a `SetUp` and `TearDown` approach.

## Aknowledgement
Problem Statement: Copyright &copy; 2004-2021 Steinar Overbeck Cook - all rights reserved.

Solution: Copyright &copy; 2023 Khan Mohammad R. - all rights reserved.

Any suggestion, pull request or bug report would be hiighly appreceated!
