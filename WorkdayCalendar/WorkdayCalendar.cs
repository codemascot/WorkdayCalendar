﻿using WorkdayCalendar.Interface;
using WorkdayCalendar.Extensions;

namespace WorkdayCalendar
{
    public sealed class WorkdayCalendar : IWorkdayCalendar
    {
        // Lock objects for synchronization
        private readonly object holidaysLock = new object();
        private readonly object recurringHolidaysLock = new object();
        private readonly object workdayStartStopLock = new object();

        public HashSet<DateTime> holidays { get; private set; }
        public HashSet<int> recurringHolidays { get; private set; }
        public TimeSpan workdayStart { get; private set; }
        public TimeSpan workdayStop { get; private set; }

        public WorkdayCalendar()
        {
            holidays = new HashSet<DateTime>();
            recurringHolidays = new HashSet<int>();
        }

        public void SetHoliday(DateTime date)
        {
            lock (holidaysLock)
            {
                holidays.Add(date.Date);
            }
        }

        public void SetRecurringHoliday(DateTime date)
        {
            lock (recurringHolidaysLock)
            {
                recurringHolidays.Add(date.KeyFromMonthAndDay());
            }
        }

        public void SetWorkdayStartAndStop(DateTime start, DateTime stop)
        {
            if (start >= stop)
            {
                throw new ArgumentException("Start time must be before stop time.");
            }

            lock (workdayStartStopLock)
            {
                workdayStart = start.TimeOfDay;
                workdayStop = stop.TimeOfDay;
            }
        }

        public DateTime GetWorkdayIncrement(DateTime start, float inc)
        {
            DateTime date = new DateTime(
                start.Date.Year,
                start.Date.Month,
                start.Date.Day,
                workdayStart.Hours,
                workdayStart.Minutes,
                0
            );

            int timeAdj = (int)(start.TimeOfDay - workdayStart).TotalMinutes;
            int workMins = (int)(workdayStop - workdayStart).TotalMinutes;

            if (inc == 0)
            {
                return timeAdj > workMins ? date.AddDays(1) : timeAdj > 0 ? start : date;
            }

            int daysInc = inc > 0 ? 1 : inc < 0 ? -1 : 0;
            int daysToAdd = (int)Math.Abs(inc);
            float remInc = Math.Abs(inc) - daysToAdd;
            int timeInc = remInc > 0 ? (int)(remInc * workMins) : 0;

            timeInc = inc < 0 && daysToAdd < 1 ? -timeInc : timeInc; // Back in the same day.
            timeInc = inc < 0 && daysToAdd > 0 ? workMins - timeInc : timeInc; // Back 1 or more days.
            timeInc = timeAdj > 0 && timeAdj < workMins ? timeInc + timeAdj : timeInc;

            if (inc > 0 && timeAdj > workMins || timeInc > workMins)
            {
                daysToAdd++;
                timeInc = timeInc > workMins ? timeInc - workMins : timeInc; // Adjusting the time when workday is over for calculated time.
            }

            if (daysToAdd == 0 && timeInc < 0) // If time is less than the start of the workday.
            {
                daysToAdd--;
                timeInc = timeInc + workMins;
            }

            while (daysToAdd > 0) // Days increment or decrement.
            {
                date = date.AddDays(daysInc);

                if (!date.IsWeekend() && !date.IsHoliday(this))
                {
                    daysToAdd--;
                }
            }

            return date.AddMinutes(timeInc); // Minutes increment or decrement.
        }
    }
}
