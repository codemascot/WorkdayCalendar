using WorkdayCalendar.Interface;

namespace WorkdayCalendar.Extensions
{
    public static class WorkdayCalendarExtensions
    {
        public static int KeyFromMonthAndDay(this DateTime date)
        {
            return date.Day * 100 + date.Month;
        }

        public static bool IsWeekend(this DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;
        }

        public static bool IsHoliday(this DateTime date, IWorkdayCalendar calendar)
        {
            return calendar.holidays.Contains(date.Date) || calendar.recurringHolidays.Contains(date.KeyFromMonthAndDay());
        }
    }
}
