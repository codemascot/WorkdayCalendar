namespace WorkdayCalendar.Interface
{
    public interface IWorkdayCalendar
    {
        public HashSet<DateTime> holidays { get; }
        public HashSet<int> recurringHolidays { get; }
        public TimeSpan workdayStart { get; }
        public TimeSpan workdayStop { get; }

        void SetHoliday(DateTime date);
        void SetRecurringHoliday(DateTime date);
        void SetWorkdayStartAndStop(DateTime start, DateTime stop);
        DateTime GetWorkdayIncrement(DateTime startDate, float incrementInWorkdays);
    }
}