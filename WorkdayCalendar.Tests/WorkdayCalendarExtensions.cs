using Moq;
using WorkdayCalendar.Interface;

namespace WorkdayCalendar.Extensions.UnitTests.Services
{
    public class WorkdayCalendarExtensionsTests
    {
        [Theory]
        [InlineData("2023-12-01", 112)]
        [InlineData("2023-05-12", 1205)]
        [InlineData("2023-07-29", 2907)]
        public void KeyFromMonthAndDay_ReturnsCorrectKey(string testDate, int expectedKey)
        {
            // Arrange
            DateTime date = DateTime.Parse(testDate);

            // Act
            int key = date.KeyFromMonthAndDay();

            // Assert
            Assert.Equal(expectedKey, key);
        }

        [Theory]
        [InlineData("2023-05-27", true)] // Saturday
        [InlineData("2023-05-28", true)] // Sunday
        [InlineData("2023-05-29", false)] // Monday
        public void IsWeekend_ReturnsCorrectValue(string testDate, bool expectedIsWeekend)
        {
            // Arrange
            DateTime date = DateTime.Parse(testDate);

            // Act
            bool isWeekend = date.IsWeekend();

            // Assert
            Assert.Equal(expectedIsWeekend, isWeekend);
        }

        [Fact]
        public void IsHoliday_ReturnsTrueForHoliday()
        {
            // Arrange
            Mock<IWorkdayCalendar> calendarMock = new Mock<IWorkdayCalendar>();
            DateTime date = new DateTime(2023, 12, 25); // A holiday

            calendarMock.SetupGet(x => x.holidays).Returns(new HashSet<DateTime>{ date });
            calendarMock.SetupGet(x => x.recurringHolidays).Returns(new HashSet<int>());

            // Act
            bool isHoliday = date.IsHoliday(calendarMock.Object);

            // Assert
            Assert.True(isHoliday);
        }

        [Theory]
        [InlineData("2023-12-25", new int[] {2512, 1705}, true)] // Xmas
        [InlineData("2023-05-17", new int[] {2512, 1705}, true)] // 17 May
        [InlineData("2023-05-29", new int[] {}, false)] // Monday
        public void IsRecurrentHoliday_ReturnsTrueForHolidayFalseForNotHoliday(string testDate, int[] reHolidays, bool expectedIsHoliday)
        {
            // Arrange
            Mock<IWorkdayCalendar> calendarMock = new Mock<IWorkdayCalendar>();
            HashSet<int> recurrentHolidays = new HashSet<int>( reHolidays );
            DateTime date = DateTime.Parse(testDate);

            calendarMock.SetupGet(x => x.holidays).Returns(new HashSet<DateTime>());
            calendarMock.SetupGet(x => x.recurringHolidays).Returns(new HashSet<int>( recurrentHolidays ));

            // Act
            bool isHoliday = date.IsHoliday(calendarMock.Object);

            // Assert
            Assert.Equal(expectedIsHoliday, isHoliday);
        }
    }
}
