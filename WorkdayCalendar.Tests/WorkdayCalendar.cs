using Moq;
using WorkdayCalendar.Interface;

namespace WorkdayCalendar.Tests
{
    public class WorkdayCalendarTests
    {
        [Fact]
        public void SetHoliday_GetsCalled_AtLeastOnce()
        {
            // Arrange
            Mock<IWorkdayCalendar> calendarMock = new Mock<IWorkdayCalendar>();
            DateTime date = new DateTime(2023, 5, 21);

            // Act
            calendarMock.Object.SetHoliday(date);

            // Assert
            calendarMock.Verify(c => c.SetHoliday(date), Times.AtLeastOnce);
        }

        [Fact]
        public void SetRecurringHoliday_GetsCalled_AtLeastOnce()
        {
            // Arrange
            Mock<IWorkdayCalendar> calendarMock = new Mock<IWorkdayCalendar>();
            DateTime date = new DateTime(2023, 5, 25);

            // Act
            calendarMock.Object.SetRecurringHoliday(date);

            // Assert
            calendarMock.Verify(c => c.SetRecurringHoliday(date), Times.AtLeastOnce);
        }

        [Fact]
        public void SetWorkdayStartAndStop_GetsCalled_AtLeastOnce()
        {
            // Arrange
            Mock<IWorkdayCalendar> calendarMock = new Mock<IWorkdayCalendar>();
            DateTime start = new DateTime(2023, 1, 1, 9, 0, 0);
            DateTime stop = new DateTime(2023, 1, 1, 17, 0, 0);

            // Act
            calendarMock.Object.SetWorkdayStartAndStop(start, stop);

            // Assert
            calendarMock.Verify(c => c.SetWorkdayStartAndStop(start, stop), Times.AtLeastOnce);
        }

        [Fact]
        public void SetHoliday_Should_SetHoliday()
        {
            // Arrange
            IWorkdayCalendar calendar = new WorkdayCalendar();
            DateTime date = new DateTime(2023, 5, 21);

            // Act
            calendar.SetHoliday(date);

            // Assert
            Assert.Contains(date.Date, calendar.holidays);
        }

        [Fact]
        public void SetRecurringHoliday_Should_SetMonthAndDayKeyToRecurringHolidays()
        {
            // Arrange
            IWorkdayCalendar calendar = new WorkdayCalendar();
            DateTime date = new DateTime(2023, 5, 25); // 2505

            // Act
            calendar.SetRecurringHoliday(date);

            // Assert
            Assert.Contains(2505, calendar.recurringHolidays);
        }

        [Fact]
        public void SetWorkdayStartAndStop_StartAfterStop_ShouldThrow_ArgumentException()
        {
            // Arrange
            IWorkdayCalendar calendar = new WorkdayCalendar();
            DateTime start = new DateTime(2023, 1, 1, 12, 0, 0); // Start time after stop time
            DateTime stop = new DateTime(2023, 1, 1, 10, 0, 0);

            // Act and Assert
            Assert.Throws<ArgumentException>(() => calendar.SetWorkdayStartAndStop(start, stop));
        }

        [Theory]
        [InlineData("2004-01-01 08:00", "2004-01-01 16:00", new string[]{"2004-05-17"}, new string[]{"2004-05-27"}, "2004-05-24 18:05", -5.5f, "2004-05-14 12:00")]
        [InlineData("2004-01-01 08:00", "2004-01-01 16:00", new string[]{"2004-05-17"}, new string[]{"2004-05-27"}, "2004-05-24 19:03", 44.723656f, "2004-07-27 13:47")]
        [InlineData("2004-01-01 08:00", "2004-01-01 16:00", new string[]{"2004-05-17"}, new string[]{"2004-05-27"}, "2004-05-24 18:03", -6.7470217f, "2004-05-13 10:02")]
        [InlineData("2004-01-01 08:00", "2004-01-01 16:00", new string[]{"2004-05-17"}, new string[]{"2004-05-27"}, "2004-05-24 08:03", 12.782709f, "2004-06-10 14:18")]
        [InlineData("2004-01-01 08:00", "2004-01-01 16:00", new string[]{"2004-05-17"}, new string[]{"2004-05-27"}, "2004-05-24 07:03", 8.276628f, "2004-06-04 10:12")]
        [InlineData("2004-01-01 08:00", "2004-01-01 16:00", new string[]{"2004-05-17"}, new string[]{"2004-05-27"}, "2004-05-24 15:07", 0.25f, "2004-05-25 09:07")]
        [InlineData("2004-01-01 08:00", "2004-01-01 17:00", new string[]{"2004-05-17"}, new string[]{"2004-05-27"}, "2004-05-24 15:07", 0.25f, "2004-05-25 08:22")]
        [InlineData("2004-01-01 08:00", "2004-01-01 16:00", new string[]{"2004-05-17"}, new string[]{"2004-05-27"}, "2004-05-24 15:07", -0.25f, "2004-05-24 13:07")]
        [InlineData("2004-01-01 08:00", "2004-01-01 16:00", new string[]{"2004-05-17"}, new string[]{"2004-05-27"}, "2004-05-24 15:07", -0.95f, "2004-05-24 15:31")]
        [InlineData("2004-01-01 08:00", "2004-01-01 16:00", new string[]{"2004-05-17"}, new string[]{"2004-05-27"}, "2004-05-24 18:03", 0f, "2004-05-25 08:00")]
        [InlineData("2004-01-01 08:00", "2004-01-01 16:00", new string[]{"2004-05-17"}, new string[]{"2004-05-27"}, "2004-05-24 14:03", 0f, "2004-05-24 14:03")]
        [InlineData("2004-01-01 08:00", "2004-01-01 16:00", new string[]{"2004-05-17"}, new string[]{"2004-05-27"}, "2004-05-24 07:03", 0f, "2004-05-24 08:00")]
        public void GetWorkdayIncrement_ReturnsCorrectDate(string wdStart, string wdEnd, string[] recHolidays, string[] holidays, string start, float inc, string expResult)
        {
            // Arrange
            IWorkdayCalendar calendar = new WorkdayCalendar();

            calendar.SetWorkdayStartAndStop(DateTime.Parse(wdStart), DateTime.Parse(wdEnd));

            foreach (string holiday in holidays)
            {
                calendar.SetHoliday(DateTime.Parse(holiday));
            }

            foreach (string recHoliday in recHolidays)
            {
                calendar.SetRecurringHoliday(DateTime.Parse(recHoliday));
            }

            DateTime startDate = DateTime.Parse(start);

            DateTime expectedDate = DateTime.Parse(expResult);

            // Act
            DateTime result = calendar.GetWorkdayIncrement(startDate, inc);

            // Assert
            Assert.Equal(expectedDate, result);
        }

    }
}
